package com.utile.test;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.util.PasswordEncoder;

public class PasswordEncoderTest {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testEncryption() {
		String password = "test";
		PasswordEncoder passwordEncoder = new PasswordEncoder();
		String encrypt;
		try {
			encrypt = passwordEncoder.encrypt(password);
			String decrypt = passwordEncoder.decrypt(encrypt);
			assertEquals(password, decrypt);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Test
	public void caseSensitive() {
		String password = "test";
		String casePassword = "TEST";
		PasswordEncoder passwordEncoder = new PasswordEncoder();
		try {
			String e1 = passwordEncoder.encrypt(password);
			String e2 = passwordEncoder.encrypt(casePassword);
			assertNotEquals(e1, e2);
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}
	@Test
	public void passwordSize() {
		String password = "test";

		PasswordEncoder passwordEncoder = new PasswordEncoder();
		try {
			String e1 = passwordEncoder.encrypt(password);
			assertTrue(true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}
	@Test
	public void passwordSize2() {
		String password = "test2222222";
		PasswordEncoder passwordEncoder = new PasswordEncoder();
		try {
			String e1 = passwordEncoder.encrypt(password);
			assertTrue(true);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

}
