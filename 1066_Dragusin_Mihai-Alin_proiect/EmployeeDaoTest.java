package com.dao.test;

import static org.junit.Assert.*;

import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Timed;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dao.CriteriaDao;
import com.dao.DepartmentDao;
import com.dao.EmployeeDao;
import com.dao.RoleDao;
import com.domain.Criteria;
import com.domain.Department;
import com.domain.Employee;
import com.domain.Role;
import com.util.SearchCriteriaObserver;
import com.util.SearchCriterias;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context.xml" })
public class EmployeeDaoTest {
	@Autowired
	private CriteriaDao criteriaDao;
	@Autowired
	private EmployeeDao employeeDao;
	@Autowired
	private DepartmentDao deparmentDao;
	@Autowired
	private RoleDao roleDao;
	SearchCriterias searchCriterias;
	SearchCriteriaObserver searchCriteriaObserver;

	@Before
	public void load() {
		searchCriterias = new SearchCriterias();
		searchCriteriaObserver = new SearchCriteriaObserver();
		searchCriterias.addObserver(searchCriteriaObserver);
		Set<Department> deparments = new HashSet<>();
		Set<Criteria> criterias = new HashSet<>();
		Department deparment = deparmentDao.findEntityById(new Long(2));
		deparments.add(deparment);
		Criteria criteria = criteriaDao.findEntityById(new Long(1));
		criterias.add(criteria);
		// searchCriterias.setDeparments(deparments);
		searchCriterias.setCriterias(criterias);
	}

	@Test
	public void testAdvanceFilterImplements() {
		// this does not work since
		Set<Employee> emps = employeeDao.getEmployeeBySearchCriterias(searchCriterias, searchCriteriaObserver);
		Employee employee = employeeDao.findEntityById(new Long(2));
		for (Employee emp : emps) {
			System.out.println(employee.getFirstName() + employee.getLastName());
		}
		if (emps.isEmpty())
			System.out.println("Result Set is null");
	}

	@Test
	public void testEmployeeDaoByName() {
		Employee employee = employeeDao.getEmployeeByEmail("alinmihai06@gmail.com");
		Employee employee2 = employeeDao.getEmployeeByEmail("alinmihai06@gmail.com");
		assertEquals(employee, employee2);
	}

	@Test
	public void getEmployeesByRole() {
		Role role = roleDao.getRoleById(new Long(2));
		Set<Employee> emps = employeeDao.getAllEmployeesWithRole(role);
		Employee employee = employeeDao.getEmployeeByName("Victor Dimbean");
		assertTrue(emps.contains(employee));
	}

	@Test
	public void getEmployeeByName() {
		Role role = roleDao.getRoleById(new Long(1));
		Set<Employee> emps = employeeDao.getAllEmployeesWithRole(role);
		Employee employee = employeeDao.getEmployeeByName("Victor Dimbean");
		System.out.println(employee.getFirstName());

	}

	@Test
	@Timed(millis = 100)
	public void getAllEmployeesTimeOut() {
		assertTrue(employeeDao.getUnemployeed().size() > 0);

	}
}
