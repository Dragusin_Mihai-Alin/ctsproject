package com.utile.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.dao.test.EmployeeDaoTest;

@RunWith(Suite.class)
@SuiteClasses({ CriteriaServiceTest.class, EqualityManagerTest.class ,PasswordEncoderTest.class, PDFCreatorTest.class,EmployeeDaoTest.class})
public class AllTests {

}
