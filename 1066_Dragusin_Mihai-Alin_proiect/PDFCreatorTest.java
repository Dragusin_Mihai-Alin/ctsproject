package com.utile.test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;

import java.io.FileNotFoundException;
import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Timed;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.domain.Employee;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.service.EmployeeService;
import com.util.PDFReportWriter;
import com.util.Pair;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context.xml" })
public class PDFCreatorTest {
	static PDFReportWriter pdfWriter;
	@Autowired
	EmployeeService employeeService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		pdfWriter = new PDFReportWriter();
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	// test document already existing
	@Test
	public void testSameDocumentCreated() {
		PDFReportWriter pdfReportWriter = new PDFReportWriter();
		assertFalse(pdfReportWriter.createDocument("Alin.pdf"));
	}

	// test bytes report stream
	@Test
	public void getBytesFromPDF() {
		PDFReportWriter.generateStreamResponse("Reports\\Alin.pdf");
	}

	// test creation pdf
	@Test
	public void testCreatePDF() {
		PDFReportWriter pdfReportWriter = new PDFReportWriter();
		assertTrue(pdfReportWriter.createDocument("Alin.pdf"));
	}

	// test null key in report returning
	@Test
	public void testNullKeyException() {
		PDFReportWriter pdfReportWriter = new PDFReportWriter();
		pdfReportWriter.getReportByName(null);
		assertTrue(true);
	}

	// test performance
	@Test
	@Timed(millis = 300)
	public void optimizationCreation() {
		PDFReportWriter pdfReportWriter = new PDFReportWriter();
		assertTrue(pdfReportWriter.createDocument("Stefan.pdf"));
	}

	// test for null key in stream generation
	@Test
	public void getReportStreamFromNonExistinPDF() {
		PDFReportWriter pdfReportWriter = new PDFReportWriter();
		byte[] result = pdfReportWriter.generateStreamResponse(null);
		assertTrue(result.length > 0);

	}

	// same key must return same reports
	@Test
	public void sameReporstBySameKey() {
		Pair<Boolean, Document> p1 = pdfWriter.getReportByName("Alin.pdf");
		Pair<Boolean, Document> p2 = pdfWriter.getReportByName("Alin.pdf");
		assertSame(p1, p2);

	}

	// different key must return different reports
	@Test
	public void differentReportsDifferntKeys() {
		Pair<Boolean, Document> p1 = pdfWriter.getReportByName("Alin.pdf");
		Pair<Boolean, Document> p2 = pdfWriter.getReportByName("Stefan.pdf");
		assertNotSame(p1, p2);
	}

	@Test
	public void tablePDF() {
		try {
			List<Employee> emps = employeeService.getAllEmployees();
			pdfWriter.createFirstTable(emps);
			assertTrue(true);
		} catch (FileNotFoundException | DocumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
