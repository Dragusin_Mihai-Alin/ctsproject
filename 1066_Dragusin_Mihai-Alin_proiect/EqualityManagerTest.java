package com.utile.test;

import static org.junit.Assert.assertTrue;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Timed;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.designs.CriteriasEqualComand;
import com.designs.EqualityComand;
import com.domain.Criteria;
import com.service.CriteriaService;
import com.util.EqualityManager;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context.xml" })
public class EqualityManagerTest {
	private static EqualityManager<Collection<Criteria>> eqManager;
	private List<Criteria> test1;
	private List<Criteria> test2;
	@Autowired
	private CriteriaService criteriaService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		Set<Criteria> criterias1 = new HashSet<>();
		Set<Criteria> criterias2 = criterias1;
		eqManager = new EqualityManager<Collection<Criteria>>(criterias1, criterias2);
		EqualityComand setCriteriasEquals = new CriteriasEqualComand();
		eqManager.setComandType(setCriteriasEquals);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {

	}

	@Before
	public void setUp() throws Exception {
		test1 = criteriaService.getAllCriterias();
		test2 = criteriaService.getAllCriterias();
	}

	@After
	public void tearDown() throws Exception {
	}
	
	
	//testing references
	@Test
	public void sameSetEquals() {
		assertTrue(eqManager.executeComand());
	}
	//testing same values
	@Test
	public void sameValuesEquals() {
		eqManager =new EqualityManager<Collection<Criteria>>(test2, test1);
		EqualityComand collectionSameEquals = new CriteriasEqualComand();
		eqManager.setComandType(collectionSameEquals);
		assertTrue(eqManager.executeComand());
	}
	// check if the inverse is true
	public void reverseSameValuesEquals(){
		eqManager =new EqualityManager<Collection<Criteria>>(test1, test2);
		EqualityComand collectionSameEquals = new CriteriasEqualComand();
		eqManager.setComandType(collectionSameEquals);
		assertTrue(eqManager.executeComand());
	}
	//testing exception thrown
	@Test
	public void nullSetException(){
		EqualityManager<Collection<Criteria>>eqManager= new EqualityManager<Collection<Criteria>>(null, test2);
		EqualityComand collectionSameEquals = new CriteriasEqualComand();
		eqManager.setComandType(collectionSameEquals);
		eqManager.executeComand();
		assertTrue(true);
	}
	//optimization test will fail with values < 375 mill
	@Test
	@Timed(millis = 200)
	public void optimizationFunction(){
		eqManager =new EqualityManager<Collection<Criteria>>(test2, test1);
		EqualityComand collectionSameEquals = new CriteriasEqualComand();
		eqManager.setComandType(collectionSameEquals);
		assertTrue(eqManager.executeComand());
	}
}
