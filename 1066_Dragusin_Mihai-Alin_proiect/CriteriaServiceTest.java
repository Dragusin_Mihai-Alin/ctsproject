package com.utile.test;

import static org.junit.Assert.*;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.dao.CriteriaDao;
import com.domain.Criteria;
import com.domain.Department;
import com.service.CriteriaService;
import com.service.DepartmentService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:context.xml" })
public class CriteriaServiceTest {
	@Autowired
	CriteriaDao criteriaDao;
	@Autowired
	CriteriaService criteriaService;
	@Autowired
	DepartmentService departmentService;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	// same criteria by id should be equals
	@Test
	public void testEqualCriterias() {
		Criteria criteria = criteriaDao.findEntityById(new Long(1));
		Criteria criteria2 = criteriaDao.findEntityById(new Long(1));
		assertEquals(criteria, criteria2);

	}
	//different criterias should be not equal
	@Test
	public void testEqualCriteriasNotEqual(){
		Criteria criteria = criteriaDao.findEntityById(new Long(1));
		Criteria criteria2 = criteriaDao.findEntityById(new Long(2));
		assertNotEquals(criteria, criteria2);
	}

	// same criteria by id should not be the same in memory
	@Test
	public void testNotSame() {
		Criteria c = criteriaService.getCriteriaById(new Long(1));
		Criteria c2 = criteriaService.getCriteriaById(new Long(2));
		assertNotSame(c, c2);
	}
	// test if method get criterias by department returns

	@Test
	public void getCriteriasByDepartment() {
		Department department = departmentService.getDepartmentById(new Long(1));
		Map<String, Criteria> criterias = criteriaService.getCriteriaMapByDepartment(department);
		boolean result = true;
		for (String s : criterias.keySet()) {
			Criteria c = criterias.get(s);
			List<Criteria> list = department.getCriterias();
			if (!list.contains(c))
				result = false;

		}
		assertTrue(result);
	}

	// search criteria by name
	@Test
	public void getCriteriaByName() {
		String name = "Munca in echipa";
		Criteria criteria = criteriaService.getCriteriaByName(name);
		assertTrue(criteria != null);
	}

	// search by name is not case sensitive
	@Test
	public void notCaseSensitiveCriteria() {
		String name = "munca in echipa";
		Criteria criteria = criteriaService.getCriteriaByName(name);
		assertTrue(criteria != null);
	}
	//there should be no criteria without a department added to
	@Test
	public void getAllCriterias() {
		Set<Department> departments = departmentService.getALlDeparments();
		int numberOfCriterias=0;
		for (Department department : departments) {
			numberOfCriterias+=department.getCriterias().size();
		}
		List<Criteria>list=criteriaService.getAllCriterias();
		assertEquals(list.size(), numberOfCriterias);
	}
}
